import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  constructor(public http: Http) {

  }
  sentment_analysis(domain: string,fasttext:string) {
    let formdata: FormData = new FormData();

    formdata.append('domain', domain);
    formdata.append('fastText',fasttext);

    return this.http.post('http://localhost:8080/sentimentAnalysis', formdata).pipe(map(res => res.json()));
  }

}

