import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-nlp',
  templateUrl: './nlp.component.html',
  styleUrls: ['./nlp.component.css']
})
export class NlpComponent implements OnInit {

  domain: string;
  file: File = null;
  fasttext:string;
  constructor(private _router: Router, private http: HttpClient) {


  }

  ngOnInit() {
  }

  onFileSelected(event) {

    this.file = <File>event.target.files[0];
    console.log(this.file)
  }
  save() {
    const fd = new FormData();
    console.log(this.fasttext);
    fd.append('file', this.file, this.file.name);
    this.http.post('http://localhost:8080/document', fd).subscribe(res => {
      
      this._router.navigate(['/sentiment_analysis', { domain: this.domain,fasttext:this.fasttext }]);
    });

  }
}
