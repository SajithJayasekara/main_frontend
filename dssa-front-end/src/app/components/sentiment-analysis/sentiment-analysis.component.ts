import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../../services/main-service.service';
import { ActivatedRoute, Params } from '@angular/router'
@Component({
  selector: 'app-sentiment-analysis',
  templateUrl: './sentiment-analysis.component.html',
  styleUrls: ['./sentiment-analysis.component.css']
})
export class SentimentAnalysisComponent implements OnInit {

  w_d_s_valueSWN: number;
  w_d_s_valueHybrid: number;
  messageSWN: String;
  messageHybrid: String;
  postive_Probability: number;
  negative_Probability: number;
  neutral_Probability: number;
  postive_ProbabilitySWN: number;
  negative_ProbabilitySWN: number;
  neutral_ProbabilitySWN: number;
  SentenceListSWN: Sentence[];
  SentenceListHybrid: Sentence[];
  results: Result[];
  domain: string;
  fasttext:string;
  //file:File;
  showLoadingIndicator = true;

  constructor(private main_service: MainServiceService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.domain = this.route.snapshot.params['domain'];
    this.fasttext = this.route.snapshot.params['fasttext'];

    this.main_service.sentment_analysis(this.domain, this.fasttext).subscribe((res_results) => {

      this.results = res_results;
      for (let result of this.results) {

        if (result.type == "Hybrid") {
          this.w_d_s_valueHybrid = result.whole_sentiment_value;
          this.messageHybrid = result.message;
          this.postive_Probability = result.postive_Probability;
          this.negative_Probability = result.negative_Probability;
          this.neutral_Probability = result.neutral_Probability;
          this.SentenceListHybrid = result.sentenceList;
        }
        else if (result.type == "Sentiwordnet") {
          this.w_d_s_valueSWN = result.whole_sentiment_value;
          this.messageSWN = result.message;
          this.postive_ProbabilitySWN = result.postive_Probability;
          this.negative_ProbabilitySWN = result.negative_Probability;
          this.neutral_ProbabilitySWN = result.neutral_Probability;
          this.SentenceListSWN = result.sentenceList;
        }
      }
      this.showLoadingIndicator = false;

    });
  }


}
interface Sentence {

  id: String,
  sentimentValue: number,
  sentence: String
}

interface Result {
  whole_sentiment_value: number,
  type: String,
  message: String,
  postive_Probability: number,
  negative_Probability: number,
  neutral_Probability: number,
  sentenceList: Sentence[]
}
