import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SentimentAnalysisComponent } from './components/sentiment-analysis/sentiment-analysis.component';
import { MainServiceService } from './services/main-service.service';
import { NlpComponent } from './components/nlp/nlp.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminComponent } from './components/admin/admin.component';
import { HomeComponent } from './components/home/home.component';


const appRoutes: Routes = [
  { path: '', component:HomeComponent },
  { path: 'sentiment_analysis', component: SentimentAnalysisComponent },
  {path: 'admin_panel', component:AdminComponent},
  {path: 'analyzer', component:NlpComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    SentimentAnalysisComponent,
    NlpComponent,
    AdminComponent,
    HomeComponent,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    NgbModule.forRoot()
  ],
  providers: [MainServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
